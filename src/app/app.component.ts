
import { Platform } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AuthService } from '../app/core/service/auth.service';
import { GoogleService } from '../app/core/service/googleplus/google.service';
import { Component, OnInit } from '@angular/core';
import { FirestoreCRUDService } from './core/service/firestore/firestore-crud.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss']
})
export class AppComponent implements OnInit {
  dataUser = this.CRUD.dataUser;
  Photo: string;
  Name: string;
  pages: any; 
  public userIsRegistered:boolean=false ;

  public selectedIndex = 0;
  public appPages = [
    {
      title: 'Inicio',
      url: 'home',
      icon: 'Home'
    },
    {
      title: 'Nosotros',
      url: 'aboutus',
      icon: 'heart'
    },
    {
      title: 'Registro de proyecto',
      url: 'projectregistrationform',
      icon: 'add-circle'
    },
    {
      title: 'Banco de proyectos',
      url: 'allprojects',
      icon: 'layers'
    },
    // {
    //   title: 'Patrocinadores',
    //   url: 'sponsors',
    //   icon: 'ribbon'
    // },
    {
      title: 'Huella de carbono',
      url: 'carbonfootprintform',
      icon: 'leaf'
    },
    // {
    //   title: 'Beneficios',
    //   url: 'benefits',
    //   icon: 'pricetag'
    // },
    {
      title: 'Plan acción 10R',
      url: 'plan-accion',
      icon: 'today'
    },
    // {
    //   title: 'Noticias',
    //   url: 'news',
    //   icon: 'newspaper'
    // },
    // {
    //   title: 'Mi cuenta',
    //   url: 'account',
    //   icon: 'person'
    // },
    // {
    //   title: 'Contáctenos',
    //   url: 'contactus',
    //   icon: 'call'
    // },
    {
      title: 'Iniciar sesión',
      url: 'login',
      icon: 'log-in'
    }
  ];
  constructor(
    private platform: Platform, private splashScreen: SplashScreen,
    private statusBar: StatusBar, private authService: AuthService,
    private googleService: GoogleService, private CRUD: FirestoreCRUDService
  ) {
    this.initializeApp();
  }
  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
  ngOnInit() {
    const path = window.location.pathname.split('home')[1];
    if (path !== undefined) {
      this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    }
    this.getCurrentUser();
  }
  //Dar función a cerrar sesión 
  onLogout(selectedIndex) {
    if (selectedIndex == 'Cerrar sesión') {
      this.googleService.logout();
      this.selectedIndex = 0;
    }
  }
  //Obtener datos del usuario
  getCurrentUser() {
    this.authService.user$.subscribe(data => {
      if (data) {
        this.appPages.pop();
        this.appPages.push(
          {
            title: 'Cerrar sesión',
            url: '/home',
            icon: 'log-out',
          }
        )
        this.userIsRegistered = true;
      } else {
        this.appPages.pop();
        this.appPages.push(
          {
            title: 'Iniciar sesión',
            url: 'login',
            icon: 'log-in'
          }
        )
        this.userIsRegistered = false;
      }
    })
  }
}