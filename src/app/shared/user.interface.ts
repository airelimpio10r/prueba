//Datos basicos del usuario registrado
export interface User {
    uid?: string;
    name?: string;
    lastName?: String;
    photoProfile?: PhotoProfile;
    email?: string;
    nit?: string;
    companyName?: string;
    companyActivity?: string;
    documentType?: string;
    document?: string;
    cellPhone?: string;
    direction?: string;
    rol?: string;
    country?: string;
    department?: string;
    municipality?: string;
    telephone?: string;
}
export interface PhotoProfile{
    photoURL?: string;
    uidPhoto?: string;
}