import { TestBed } from '@angular/core/testing';

import { LogicReuseService } from './logic-reuse.service';

describe('LogicReuseService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LogicReuseService = TestBed.get(LogicReuseService);
    expect(service).toBeTruthy();
  });
});
