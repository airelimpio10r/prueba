export interface Sponsor {
    quantity: string, // FIXME: UNA MEJOR DESCRIPCION 
    amountSponsor: string,
    sponsorshipStart: string,
    endSponsorship: string,
    amountMoney: number,
    contributionFrequency: string,
    projectName: string,
    user: User;

}

export interface User {
    email: string;
}