export interface RegisterProject {
    projectType: string;
    projectName: string;
    forestArea: string;
    forestType: string;
    temperature: string;
    typeTemperature: string;
    treeDiameter: string;
    treeAge: string;
    treeSize: string;
    country: string;
    department: string;
    municipality: string;
    zone: string;
    msnm: string;
    propertyName: string;
    cadastralCertificate: string;
    enrollment: string;
    user: User;
    photos?: IType;
}
export interface IType{
    [key: string]: string;
}
export interface User {
    email: string;
}