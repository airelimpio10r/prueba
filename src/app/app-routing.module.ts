import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '../app/core/guards/auth.guard';
import { NoLoginGuard } from '../app/core/guards/no-login.guard'

const routes: Routes = [
  {
    path: '',
    redirectTo: 'home', // Modificando esto se puede cambiar la pagina que aparece despues de la carga inicial
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./public/home/home.module').then( m => m.HomePageModule)
  },
 
  {
    path: 'login',
    children:[
      {
        path: '',
        loadChildren: () => import('./public/authentication/login/login.module').then( m => m.LoginPageModule),
        canActivate:[NoLoginGuard],
      },
      {
        path: 'registry',
        loadChildren: () => import('./public/authentication/registry/registry.module').then( m => m.RegistryPageModule),
        canActivate:[NoLoginGuard],
      },
      {
        path: 'recoverpassword',
        loadChildren: () => import('./public/authentication/recoverpassword/recoverpassword.module').then( m => m.RecoverpasswordPageModule),
        canActivate:[NoLoginGuard],
      },
    ]
  },
  {
    path: 'aboutus',
    loadChildren: () => import('./public/aboutus/aboutus.module').then( m => m.ABOUTUSPageModule)
  },
  {
    path: 'contactus',
    loadChildren: () => import('./public/contactus/contactus.module').then( m => m.ContactusPageModule)
  },
  {
    path: 'carbonfootprintform',
    loadChildren: () => import('./public/carbonfootprintform/carbonfootprintform.module').then( m => m.CarbonfootprintformPageModule)
  },
  {
    path: 'projectregistrationform',
    loadChildren: () => import('./private/projectregistrationform/projectregistrationform.module').then( m => m.ProjectregistrationformPageModule),
    canActivate:[AuthGuard],
  },
  {
    path: 'sponsorshipform',
    loadChildren: () => import('./private/sponsorshipform/sponsorshipform.module').then( m => m.SponsorshipformPageModule),
    canActivate:[AuthGuard],
  },
  {
    path: 'account',
    loadChildren: () => import('./private/account-information/account/account.module').then( m => m.AccountPageModule),
    canActivate:[AuthGuard],
  },
  {
    path: 'allprojects',
    children:[
      {
        path:'',
        loadChildren: () => import('./public/bankofprojects/allprojects/allprojects.module').then( m => m.AllprojectsPageModule)
      },
      {
        path: 'projectsdetails',
        loadChildren: () => import('./public/bankofprojects/projectsdetails/projectsdetails.module').then( m => m.ProjectsdetailsPageModule)
      }
    ]
  },
  {
    path: 'sponsors',
    loadChildren: () => import('./public/sponsors/sponsors.module').then( m => m.SponsorsPageModule)
  },
  {
    path: 'news',
    loadChildren: () => import('./public/news/news.module').then( m => m.NewsPageModule)
  },
  {
    path: 'benefits',
    loadChildren: () => import('./public/benefits/benefits.module').then( m => m.BenefitsPageModule)
  },
  {
    path: 'admin-dashboard',
    loadChildren: () => import('./private/admin/admin-dashboard/admin-dashboard.module').then( m => m.AdminDashboardPageModule),
    canActivate:[AuthGuard],
    data:{
      role: 'Admin'
    }
  },
  {
    path: 'video-popup',
    loadChildren: () => import('./public/video-popup/video-popup.module').then( m => m.VideoPopupPageModule)
  },  {
    path: 'terms-and-conditions',
    loadChildren: () => import('./public/terms-and-conditions/terms-and-conditions.module').then( m => m.TermsAndConditionsPageModule)
  },
  {
    path: 'carbonfootprintform',
    loadChildren: () => import('./public/carbonfootprintform/carbonfootprintform.module').then( m => m.CarbonfootprintformPageModule)
  },



];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}


