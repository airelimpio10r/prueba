import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';

//Agregamos los componentes
import { EmailComposer } from '@ionic-native/email-composer/ngx'; // Impotamos componente de activacion de correo electronico
import { FormsModule, ReactiveFormsModule } from '@angular/forms'; // Componentes de la propiedad Form
//firebase config
import { AngularFirestoreModule, FirestoreSettingsToken } from "@angular/fire/firestore"; //Modulo Firestore (BD)
import { AngularFireAuthModule } from "@angular/fire/auth"; //Modulo de authenticacion
import { AngularFireModule } from "@angular/fire"; //Modulo para inicializar
import { firebaseConfig } from 'src/environments/environment'; // aqui se encuentra una variable de configuracion para inicializar firebase
import { AngularFireStorageModule } from '@angular/fire/storage';
//Componentes de autentificación
import { GooglePlus } from '@ionic-native/google-plus/ngx' // Login Google
import { Facebook } from '@ionic-native/facebook/ngx';//Login Facebook
import { AuthGuard } from './core/guards/auth.guard'
import { VideoPopupPage } from './public/video-popup/video-popup.page';
import { ModalImagePageModule } from './public/bankofprojects/projectsdetails/modal-image/modal-image.module';
import { ModalGaleryPageModule } from './public/bankofprojects/projectsdetails/modal-galery/modal-galery.module';

@NgModule({
  declarations: [AppComponent,VideoPopupPage],
  entryComponents: [VideoPopupPage],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFirestoreModule,
    AngularFireAuthModule,
    AngularFireStorageModule,
    ModalImagePageModule,
    ModalGaleryPageModule
  ],
  providers: [
    Facebook,
    GooglePlus,
    StatusBar,
    SplashScreen,
    EmailComposer,
    AuthGuard,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy },
    { provide: FirestoreSettingsToken, useValue: {} },

  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
