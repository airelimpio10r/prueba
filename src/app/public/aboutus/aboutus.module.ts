import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ABOUTUSPageRoutingModule } from './aboutus-routing.module';

import { ABOUTUSPage } from './aboutus.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ABOUTUSPageRoutingModule,
  ],
  declarations: [ABOUTUSPage]
})
export class ABOUTUSPageModule {}
