import { Component, OnInit, ViewChild } from '@angular/core';
import { IonSlides, MenuController } from '@ionic/angular';
import { Router } from '@angular/router';
import { CheckTutorialGuard } from 'src/app/core/guards/check-tutorial.guard';

@Component({
  selector: 'app-tutorial',
  templateUrl: './tutorial.page.html',
  styleUrls: ['./tutorial.page.scss'],
})
export class TutorialPage {

  showSkip = true;

  @ViewChild('slides', { static: true }) slides: IonSlides;

  constructor(
    private menu: MenuController, private app: CheckTutorialGuard,
    private router: Router,
  ) {}

  startApp() {
    this.app.ckeckTutorial = true;
    this.router.navigateByUrl('/home', { replaceUrl: true });
  }

  onSlideChangeStart(event) {
    event.target.isEnd().then(isEnd => {
      this.showSkip = !isEnd;
    });
  }

  // ionViewWillEnter() {
  //   this.storage.get('ion_did_tutorial').then(res => {
  //     if (res === true) {
  //       this.router.navigateByUrl('/app/tabs/schedule', { replaceUrl: true });
  //     }
  //   });

  //   this.menu.enable(false);
  // }

  ionViewDidLeave() {
    // enable the root left menu when leaving the tutorial page
    this.menu.enable(true);
  }
}



