import { Component, OnInit} from '@angular/core';
import { ModalController} from "@ionic/angular";
@Component({
  selector: 'app-modal-image',
  templateUrl: './modal-image.page.html',
  styleUrls: ['./modal-image.page.scss'],
})
export class ModalImagePage implements OnInit {
  imagenes=[
    'forest1.jpg',
  'forest2.jpg',
  'back.jpg'
  ]

  constructor(private modalCtrl : ModalController) {}
  cerrarModal(){
    this.modalCtrl.dismiss();
  }


  ngOnInit() {
  }

}
