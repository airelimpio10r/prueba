import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProjectsdetailsPageRoutingModule } from './projectsdetails-routing.module';

import { ProjectsdetailsPage } from './projectsdetails.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProjectsdetailsPageRoutingModule,
  ],
  declarations: [ProjectsdetailsPage]
})
export class ProjectsdetailsPageModule {}
