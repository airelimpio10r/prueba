import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';
import { LogicReuseService } from 'src/app/shared/resources/logic-reuse.service';
import { ModalImagePage } from './modal-image/modal-image.page';
import { ModalGaleryPage } from './modal-galery/modal-galery.page';


@Component({
  selector: 'app-projectsdetails',
  templateUrl: './projectsdetails.page.html',
  styleUrls: ['./projectsdetails.page.scss'],
})
export class ProjectsdetailsPage implements OnInit {


  
  constructor(private modalCtrl : ModalController,
    private logicReuse: LogicReuseService,) { }
  // popUp(){
  //   this.logicReuse.alertMessaje('<img src="../assets/images/pago.jpg">')
  // }
  async popUp(){
    const modal=this.modalCtrl.create({
    component:ModalImagePage,
    cssClass:'my-modal2-css',
    
 });
 return (await modal).present()
  }
  async popUpGalery(){
    const modal=this.modalCtrl.create({
    component:ModalGaleryPage,
    cssClass:'my-modal-galery-css',
    
 });
 return (await modal).present()
  }
  ngOnInit() {
  }

}
