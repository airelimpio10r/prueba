import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProjectsdetailsPage } from './projectsdetails.page';

const routes: Routes = [
  {
    path: '',
    component: ProjectsdetailsPage
  },  {
    path: 'modal-galery',
    loadChildren: () => import('./modal-galery/modal-galery.module').then( m => m.ModalGaleryPageModule)
  },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProjectsdetailsPageRoutingModule {}
