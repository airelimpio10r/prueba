import { Component, OnInit } from '@angular/core';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-modal-galery',
  templateUrl: './modal-galery.page.html',
  styleUrls: ['./modal-galery.page.scss'],
})
export class ModalGaleryPage implements OnInit {
  imagenes=[
    'forest1.jpg',
  'forest2.jpg',
  'back.jpg',
  'colorful2.jpg',
  'forestt2.jpg',
  'forest1.jpg',
  'forest2.jpg',
  'back.jpg',
  
   ]
  constructor(private modalCtrl : ModalController) { }
  cerrarModal(){
    this.modalCtrl.dismiss();
  }

  ngOnInit() {
  }

}
