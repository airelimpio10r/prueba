import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AllprojectsPage } from './allprojects.page';

const routes: Routes = [
  {
    path: '',
    component: AllprojectsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AllprojectsPageRoutingModule {}
