import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CarbonfootprintformPageRoutingModule } from './carbonfootprintform-routing.module';

import { CarbonfootprintformPage } from './carbonfootprintform.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CarbonfootprintformPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [CarbonfootprintformPage]
})
export class CarbonfootprintformPageModule {}

