import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { CarbonfootprintformPage } from './carbonfootprintform.page';

const routes: Routes = [
  {
    path: '',
    component: CarbonfootprintformPage
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CarbonfootprintformPageRoutingModule {}
