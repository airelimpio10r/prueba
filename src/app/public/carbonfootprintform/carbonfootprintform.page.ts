import { Component, OnInit, ViewChild, ChangeDetectorRef } from '@angular/core';
import { IonSlides } from '@ionic/angular';
import { Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'app-carbonfootprintform',
  templateUrl: './carbonfootprintform.page.html',
  styleUrls: ['./carbonfootprintform.page.scss'],
})
export class CarbonfootprintformPage implements OnInit {

  CalculoHuellaCarbono: number;
  CalculodeArboles: number;
  //Forms
  carbonFootprint: FormGroup;
  //Booleanos
  disabled2 = true; disabled = true; Display = true; Display4 = true; 
  hidden = true; hidden1 = true; hidden2 = true; hidden3 = true;
  Ocultar = true; Ocultar1 = true; checked: boolean; checked2: boolean;
  // Invoca la propiedad IonSlides y permite modificarla
  @ViewChild('slides', { static: false }) slides: IonSlides;
  // Caracteristicas de los slides
  options = {
    lockSwipes: true, // Bloquea los slides
    onlyExternal: false, // Deshabilita el deslisamiento 
    // aqui inicializa los slides
    onInit: (slides: any) =>
      this.slides = slides
  }
  //Items de los select del transporte terrestre 
  MedioDeTrasporte = [
    'Bicicleta', 'Moto', 'Autobús', 'Automóvil'
  ]
  TipodeCombustible = [
    'Gasolina', 'Diesel', 'Gas'
  ]
  TipodeMotor = {
    Gasolina: ['Motor < 1.4L', 'Motor > 1.4L < 2.0L', 'Motor > 2.0L'],
    Diesel: ['Motor < 1.7L', 'Motor > 1.7L < 2.0L', 'Motor > 2L'],
    Gas: ['Medio', 'Grande']
  }
  ValueMotorWithDistance = {
    'Motor < 1.4L': '0.06516', 'Motor > 1.4L < 2.0L': '0.07704', 'Motor > 2.0L': '0.10656',
    'Motor < 1.7L': '0.05436', 'Motor > 1.7L < 2.0L': '0.06768', 'Motor > 2L': '0.09288',
    'Medio': '0.06804', 'Grande': '0.09324'
  }
  ValueMotorWithDriving = {
    'Motor < 1.4L': '3.258', 'Motor > 1.4L < 2.0L': '3.852', 'Motor > 2.0L': '5.328',
    'Motor < 1.7L': '2.718', 'Motor > 1.7L < 2.0L': '3.384', 'Motor > 2L': '4.644',
    'Medio': '3.402', 'Grande': '4.662'
  }
  MotorChoose = [];
  //
  valueselect: number;

  cambiarFondo='cero';
ocultar  = true;
ocultar1  = false;
ocultar2   = false;
ocultar3    = false;
ocultar4   = false;
ocultar5    = false;
  constructor(
    private _cdr: ChangeDetectorRef, private formBuilder: FormBuilder, private router: Router,
  ) { }
  accion1() {
    this.ocultar=!this.ocultar;
    this.ocultar1 = !this.ocultar1;
    this.cambiarFondo='first'
  }

  ngOnInit() {
    this.carbonFootprint = this.FormcarbonFootprint();
  }
  // esta funcion va anclada con el formulario
  saveData(value) {

  }

  // Condiciones de la entrada de datos form
  private FormcarbonFootprint() {
    return this.formBuilder.group({
      tuDieta: [''],
      cordero: ['', Validators.pattern('[0-9]*')],
      res: ['', Validators.pattern('[0-9]*')],
      queso: ['', Validators.pattern('[0-9]*')],
      tocino: ['', Validators.pattern('[0-9]*')],
      cerdo: ['', Validators.pattern('[0-9]*')],
      camarones: ['', Validators.pattern('[0-9]*')],
      pollo: ['', Validators.pattern('[0-9]*')],
      huevos: ['', Validators.pattern('[0-9]*')],
      arroz: ['', Validators.pattern('[0-9]*')],
      chocolate: ['', Validators.pattern('[0-9]*')],
      cafe: ['', Validators.pattern('[0-9]*')],
      vino: ['', Validators.pattern('[0-9]*')],
      leche: ['', Validators.pattern('[0-9]*')],
      cerveza: ['', Validators.pattern('[0-9]*')],
      //
      valorElectrico: [''],
      Npersonas: ['', Validators.pattern('[0-9]*')],
      //
      distanciaDiaria: ['', [Validators.pattern('[0-9]*')]],
      horasdeManejo: ['', [Validators.pattern('[0-9]*')]],
      combustible: [''],
      motor: [''],
      MedioDeTrasporte: [''],
      //
      nacional: ['', [Validators.pattern('[0-9]*')]],
      norteamerica: ['', [Validators.pattern('[0-9]*')]],
      suramerica: ['', [Validators.pattern('[0-9]*')]],
      europa: ['', [Validators.pattern('[0-9]*')]],
      asia: ['', [Validators.pattern('[0-9]*')]],
      oceania: ['', [Validators.pattern('[0-9]*')]],
      africa: ['', [Validators.pattern('[0-9]*')]],
    })
  }

  //Data del Form consumo electrico
  saveConsumoElectrico(value) {
    if (this.Display == false) {
      this.CalculoHuellaCarbono = ((parseInt(value.valorElectrico) / 495.5) / (parseInt(value.Npersonas))) * 0.005724;
      //this.nextSlide();
      this.ocultar1 = !this.ocultar1;
    this.ocultar2 = !this.ocultar2;
    this.cambiarFondo='second'
    } else if (this.checked == true) {
      this.CalculoHuellaCarbono = 0.3897;
      //this.nextSlide();
      this.ocultar1 = !this.ocultar1;
    this.ocultar2 = !this.ocultar2;
    this.cambiarFondo='second'
    }
    
  }
  //Data del Form consumo alimentario
  saveConsumoAlimentario(value) {
    if (this.disabled2 == false) {
      this.CalculoHuellaCarbono += parseFloat(value.tuDieta);
      //this.nextSlide();
      this.ocultar3 = !this.ocultar3;
      this.ocultar2 = !this.ocultar2;
      this.cambiarFondo='third'
    } else if (this.disabled == false) {
      let valorcomida = ((((value.cordero * 0.039) + (value.res * 0.027) + (value.queso * 0.013) + (value.tocino * 0.0125) + (value.cerdo * 0.012) + (value.camarones * 0.012) + (value.pollo * 0.004) +
        (value.huevos * 0.004) + (value.arroz * 0.0029)) * 12) + (((value.chocolate * 0.011) + (value.cafe * 0.0079) + (value.vino * 0.003) + (value.leche * 0.0012) + (value.cerveza * 0.0012)) * 12));
      this.CalculoHuellaCarbono += valorcomida;
      //this.nextSlide();
      this.ocultar3 = !this.ocultar3;
      this.ocultar2 = !this.ocultar2;
      this.cambiarFondo='third'
    }
  }
  //Data del Form consumo Transporte Terrestre
  saveConsumoTerrestre(value) {
    let medioDeTrasporte = this.carbonFootprint.get('MedioDeTrasporte').value
    if (medioDeTrasporte == 'Automovil' || medioDeTrasporte == 'Moto') {
      if (this.hidden1 == false) {
        this.valueselect = this.ValueMotorWithDistance[value.motor];
        this.CalculoHuellaCarbono += (this.valueselect * parseInt(value.distanciaDiaria));
        this.ocultar4 = !this.ocultar4;
        this.ocultar3 = !this.ocultar3;
        this.cambiarFondo='four'
      }
      if (this.hidden2 == false) {
        this.valueselect = this.ValueMotorWithDriving[value.motor];
        this.CalculoHuellaCarbono += (this.valueselect * parseInt(value.horasdeManejo));
        this.ocultar4 = !this.ocultar4;
        this.ocultar3 = !this.ocultar3;
        this.cambiarFondo='four'
      }
    } else if (medioDeTrasporte == 'Autobús') {
      if (this.hidden1 == false) {
        this.CalculoHuellaCarbono += (0.0226 * parseInt(value.distanciaDiaria));
        this.ocultar4 = !this.ocultar4;
    this.ocultar3 = !this.ocultar3;
    this.cambiarFondo='four'
      }
      if (this.hidden2 == false) {
        this.CalculoHuellaCarbono += (1.1299 * parseInt(value.horasdeManejo));
        this.ocultar4 = !this.ocultar4;
    this.ocultar3 = !this.ocultar3;
    this.cambiarFondo='four'
      }
    }else {
      this.ocultar4 = !this.ocultar4;
      this.ocultar3 = !this.ocultar3;
      this.cambiarFondo='four'
    }
    if (this.hidden3 == false) {
      this.CalculoHuellaCarbono += 0.1;
      this.ocultar4 = !this.ocultar4;
    this.ocultar3 = !this.ocultar3;
    this.cambiarFondo='four'
    }
  }
  //Data del Form consumo Transporte Terrestre
  saveConsumoAereo(value) {
    if (this.Ocultar == false) {
      this.CalculoHuellaCarbono += ((value.nacional * 0.22500711) + (value.norteamerica * 2.290374) + (value.suramerica * 1.989009) + (value.europa * 4.520475) + (value.asia * 7.534125) + (value.oceania * 7.534125) +
        (value.africa * 7.534125));
      let calculo = Math.round(this.CalculoHuellaCarbono);
      this.CalculoHuellaCarbono = calculo;
      this.ocultar4 = !this.ocultar4;
      this.ocultar5 = !this.ocultar5;
      this.cambiarFondo='five'
    }
    if (this.Ocultar1 == false) {
      let calculo = Math.round(this.CalculoHuellaCarbono);
      this.CalculoHuellaCarbono = calculo;
      this.ocultar4 = !this.ocultar4;
      this.ocultar5 = !this.ocultar5;
      this.cambiarFondo='five'
    }
    const calculo = ((2.5001 * this.CalculoHuellaCarbono) - 0.0445);
    this.CalculodeArboles = Math.round(calculo);
  }

  // Activa el evento ionChange para introducir datos a los selects
  TipodeCombustibleChange() {
    let combustible = this.carbonFootprint.get('combustible').value; //Pide el valor del Select 
    this.MotorChoose = this.TipodeMotor[combustible]; // introduce en el Select las opciones de acuerdo a lo escogido
    document.getElementById('motor').style.display = "block";
    this.TransporteTerrestre();
    this._cdr.detectChanges();
  }
  // Pasa el slide
  nextSlide() {
    this.slides.lockSwipes(false);
    this.slides.slideNext();
    this.slides.lockSwipes(true);// bloquea los slide para que solo se muevan con el boton
  }
  // Pasa el slide
  behindSlide() {
    this.slides.lockSwipes(false);
    this.slides.slidePrev();
    this.slides.lockSwipes(true);// bloquea los slide para que solo se muevan con el boton
  }
  // Vuelve al primer slide
  firstSlide() {
    // this.slides.lockSwipes(false);
    // this.slides.slideTo(0);
    // this.slides.lockSwipes(true);
    this.ocultar = !this.ocultar;
    this.ocultar5 = !this.ocultar5;
    // this.carbonFootprint.reset();
    this.disabled2=false;
     this.disabled = false; this.Display = false; this.Display4 = false; 
     this.hidden = false; this.hidden1 = false; this.hidden2 = false; this.hidden3 = false;
     this.Ocultar = false; this.Ocultar1 = false
  }
  //desactiva o activa opciones de Consumo electrico
  hiddenConsumoElectrico() {
    if (this.Display == true) {
      document.getElementById('ElectricidadDisabled').style.display = "none"
      document.getElementById('Nose').style.display = "block";
    }
    else {
      document.getElementById('ElectricidadDisabled').style.display = "block";
      document.getElementById('Nose').style.display = "none";
    }
  }
  hiddentransporte() {
    if (this.Display4 == true) {
      document.getElementById('mediodetransporte').style.display = "none"
      document.getElementById('itemNotengo').style.display = "block";
    }
    else {
      document.getElementById('mediodetransporte').style.display = "block";
      document.getElementById('itemNotengo').style.display = "none";
    }
  }
  //desactiva o activa opciones extra de la alimentación
  hiddenMiconsumo() {
    if (this.disabled2 == true) {
      document.getElementById('tipoDieta').style.display = "none"
      document.getElementById('consumo').style.display = "block"
    } else {
      document.getElementById('tipoDieta').style.display = "block"
      document.getElementById('consumo').style.display = "none"
    }
    if (this.disabled == true) {
      document.getElementById('Miconsumo').style.display = "none"
    }
    else {
      document.getElementById('Miconsumo').style.display = "block"
    }
  }
  //desactiva o activa opciones extra de TransporteTerrestre
  TransporteTerrestre() {
    const valor = this.carbonFootprint.get('MedioDeTrasporte').value
    if (valor == 'Bicicleta') {
      document.getElementById('itemNotengo').style.display = "none";
      document.getElementById('Distancia').style.display = "none";
      document.getElementById('Manejo').style.display = "none";
    } else if (valor) {
      document.getElementById('Distancia').style.display = "block";
      document.getElementById('Manejo').style.display = "block";
    }
    if (this.hidden1 == false) {
      document.getElementById('Manejo').hidden = true;
      document.getElementById('distanciaD').style.display = "block";
      if (valor != 'Autobús') {
        document.getElementById('TransporteTerrestre').style.display = "block";
      }
    } else {
      document.getElementById('TransporteTerrestre').style.display = "none";
      document.getElementById('distanciaD').style.display = "none";
      document.getElementById('Manejo').hidden = false;
    }
    if (this.hidden2 == false) {
      document.getElementById('Distancia').hidden = true;
      document.getElementById('horasManejo').style.display = "block";
      if (valor != 'Autobús') {
        document.getElementById('TransporteTerrestre').style.display = "block";
      }
    } else {
      document.getElementById('horasManejo').style.display = "none";
      document.getElementById('Distancia').hidden = false;
    }
    if (this.hidden3 == false) {
      document.getElementById('mediodetransporte').style.display = "none";
      document.getElementById('Distancia').style.display = "none";
      document.getElementById('Manejo').style.display = "none";
    } else {
      document.getElementById('mediodetransporte').style.display = "block";
    }
  }
  //desactiva o activa opciones extra de Transporte aereo
  hiddenConsumoAereo() {
    if (this.Ocultar == false) {
      document.getElementById('NoviajoAvion').style.display = "none";
      document.getElementById('viajesCheck').style.display = "block";
    } else {
      document.getElementById('NoviajoAvion').style.display = "block";
      document.getElementById('viajesCheck').style.display = "none";
    }
    if (this.Ocultar1 == false) {
      document.getElementById('ViajoAvion').style.display = "none";
    } else {
      document.getElementById('ViajoAvion').style.display = "block";
    }
  }
}
