import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthService } from '../../../core/service/auth.service';
import { Router } from '@angular/router';
import { AppComponent } from 'src/app/app.component';
import { GoogleService } from '../../../core/service/googleplus/google.service';
import { LogicReuseService } from 'src/app/shared/resources/logic-reuse.service';
import { ModalController } from '@ionic/angular';
import { RecoverpasswordPage } from '../recoverpassword/recoverpassword.page';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  hide = true;
  loginFrom: FormGroup;
  // Mesajes de error en las entradas de datos de los forms
  validation_messages

  constructor(
    private authService: AuthService, private router: Router,
    private formBuilder: FormBuilder, private app: AppComponent,
    private googleService: GoogleService, private logicReuse: LogicReuseService,
  ) { }

  ngOnInit() {
    this.loginFrom = this.createMyForm();
    this.validation_messages = this.logicReuse.validation_messages
  }

  //Evalua el uso del botton enter para hacer el submit
  eventHandler(keyCode) {
    if (keyCode === 13) {
      if (this.loginFrom.valid) {
        // Trigger the button element with a click
        document.getElementById("bttSubmit").click();
      }
    }
  }
  // Login con Google
  async LoginGoogle() {
    try {
      this.googleService.loginWithGoogle();
    } catch (error) {
      console.log('Error', error);
    }
  }
  //Login con Firebase (Correo y contraseña)
  async loginUser(value) {
    try {
      const user = this.authService.onLogin(value);
      this.app.selectedIndex = 0;
    } catch (error) {
      console.log('Error', error);
    }
  }
  // Donde se guardan los datos del form
  saveData(data) {
    this.loginUser(data);
    this.loginFrom.reset();
  }
  // Condiciones de la entrada de datos form
  private createMyForm() {
    return this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required]]
    });
  }
}
