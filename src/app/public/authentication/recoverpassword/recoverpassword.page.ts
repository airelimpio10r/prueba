import { Component, OnInit } from '@angular/core';

import { ToastController, ModalController } from '@ionic/angular';
import { AuthService } from '../../../core/service/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';


@Component({
  selector: 'app-recoverpassword',
  templateUrl: './recoverpassword.page.html',
  styleUrls: ['./recoverpassword.page.scss'],
})
export class RecoverpasswordPage implements OnInit {
  forgotPassword: FormGroup;

  constructor(
    private toastController: ToastController, private authServise: AuthService,
    private formBuilder: FormBuilder, private router: Router,
  ) { }

  ngOnInit() {
    this.forgotPassword = this.FormforgotPassword();
  }

  //Envia link de restauración de contraseña
  sendLinkReset(value) {
    this.authServise.resetPassword(value).then(() => {
      this.presentToast();
      this.router.navigate(['/login']);
    }).catch(err => alert(err));
  }
  //Condiciones de la entrada de datos form
  private FormforgotPassword() {
    return this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]]
    })
  }
  // Mesajes de error en las entradas de datos de los forms
  validation_messages = {
    'email': [
      { type: 'required', message: 'El correo electrónico es requerido.' },
      { type: 'email', message: 'Por favor, ingrese un correo electrónico válido.' }
    ]
  }
  // Función que modifica el texto que aparecera al hacer click en el boton ok 
  async presentToast() {
    const toast = await this.toastController.create({
      message: ' <ion-icon name="checkmark-circle"></ion-icon> Se envió un correo electrónico con un enlace para reestablecer su contraseña.',
      duration: 6000,
      position: 'middle',
      color:'success',
      animated:true,
      cssClass:"my-custom-class"
    });
    toast.present();
  }
  toast() {
    this.presentToast();
  }
}
