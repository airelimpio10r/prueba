import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from '@angular/forms';
import { ToastController } from '@ionic/angular';
import { AuthService } from '../../../core/service/auth.service';
import { LogicReuseService } from 'src/app/shared/resources/logic-reuse.service';

@Component({
  selector: 'app-registry',
  templateUrl: './registry.page.html',
  styleUrls: ['./registry.page.scss'],
})
export class RegistryPage implements OnInit {
  selectTabs='naturalPerson';
  RegistryPNatural: FormGroup;
  RegistryBusiness: FormGroup;
  activate = false;
  hide: boolean;
  hide1: boolean;
  // Mesajes de error en las entradas de datos de los forms
  validation_messages;

  constructor(
    private toastController: ToastController, private authService: AuthService,
    private formBuilder: FormBuilder, private logicReuse: LogicReuseService
  ) { }

  ngOnInit() {
    this.RegistryPNatural = this.FormPNatural();
    this.RegistryBusiness = this.FormEmpresa();
    this.validation_messages = this.logicReuse.validation_messages;
  }

  //Registrar con Firebase (Contraseña y correo)
  async RegistroEmailPassword(value) {
    try {
      this.authService.onRegister(value)
    } catch (error) {
      console.log(error);
    }
  }
  // Donde se guardan los datos del form
  saveDataPNatural(data) {
    this.RegistroEmailPassword(data);
    this.RegistryBusiness.reset(); // borramos los datos del form
  }
  saveDataEmpresa(data) {
    this.RegistroEmailPassword(data);
    this.RegistryPNatural.reset(); // borramos los datos del form
  }
  //Evalua el uso del botton enter para hacer el submit 
  eventHandler(keyCode) {
    if (keyCode === 13) {
      if (this.RegistryPNatural.valid) {
        // Trigger the button element with a click
        document.getElementById("bttSubmitPNatural").click();
      }
    }
  }
  EventHandler(keyCode) {
    if (keyCode === 13) {
      if (this.RegistryBusiness.valid) {
        // Trigger the button element with a click
        document.getElementById("bttSubmitEmpresa").click();
      }
    }
  }
  // Verifica la contraseña en el form
  matchOtherValidator(otherControlName: string) {
    let thisControl: FormControl;
    let otherControl: FormControl;
    return function matchOtherValidate(control: FormControl) {
      if (!control.parent) {
        return null;
      }
      // Initializing the validator.
      if (!thisControl) {
        thisControl = control;
        otherControl = control.parent.get(otherControlName) as FormControl;
        if (!otherControl) {
          throw new Error('matchOtherValidator(): other control is not found in parent group');
        }
        otherControl.valueChanges.subscribe(() => {
          thisControl.updateValueAndValidity();
        });
      }
      if (!otherControl) {
        return null;
      }
      if (otherControl.value !== thisControl.value) {
        return {
          matchOther: true
        };
      }
      return null;
    }
  }
  // Condiciones de la entrada de datos form
  private FormEmpresa() {
    return this.formBuilder.group({
      nit: ['', [Validators.required, Validators.minLength(2)]],
      companyName: ['', [Validators.required, Validators.minLength(2)]],
      companyActivity: ['', [Validators.required]],
      name: ['', [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(2)]],
      lastName: ['', [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(2)]],
      cellPhone: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10), Validators.pattern('[0-9]*')]],
      documentType: ['', Validators.required],
      document: ['', [Validators.required, Validators.minLength(8), Validators.pattern('[A-Za-z0-9]*')]],
      direction: ['', [Validators.required, Validators.minLength(2)]],
      email: ['', [Validators.required, Validators.email]],
      passwordRetry: this.formBuilder.group({
        password: ['', [Validators.required, Validators.minLength(6), Validators.pattern('(?=.*\\d)(?=.*[0-9])(?=.*[a-záéíóúüñ]).*[A-ZÁÉÍÓÚÜÑ].*')]],
        passwordConfirmation: ['', [Validators.required, Validators.minLength(6), this.matchOtherValidator('password')]]
      }),
      termsAndConditions: ['', [Validators.required]],
    });
  }
  // Condiciones de la entrada de datos form
  private FormPNatural() {
    return this.formBuilder.group({
      name: ['', [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(2)]],
      lastName: ['', [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(2)]],
      email: ['', [Validators.required, Validators.email]],
      cellPhone: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10), Validators.pattern('[0-9]*')]],
      passwordRetry: this.formBuilder.group({
        password: ['', [Validators.required, Validators.minLength(6), Validators.pattern('(?=.*\\d)(?=.*[0-9])(?=.*[a-záéíóúüñ]).*[A-ZÁÉÍÓÚÜÑ].*')]],
        passwordConfirmation: ['', [Validators.required, Validators.minLength(6), this.matchOtherValidator('password')]]
      }),
         termsAndConditions: ['', [Validators.required]],
    });
  }
  // Función que mostrara un texto al hacer click o enter  en correo
  async presentToast1() {
    const toast = await this.toastController.create({
      message: 'Su correo electronico sera usado como usuario en el inicio de sesión.',
      duration: 4000,
      position: 'middle'
    });
    toast.present();
  }
  Event(keyCode) {
    if (keyCode === 13) {
      this.presentToast1();
    }
  }
  //Función del boton Empresas esconde lo de Natural y muestra lo de Empresas
  // bttRegistryBusiness() {
  //   document.getElementById('naturalPerson').style.display = "none";
  //   document.getElementById('business').style.display = "block";
  //   if (this.activate == false) {
  //     this.activate = true;
  //   }
  // }
  // Función del boton Natural esconde lo de Empresas y muestra lo de Natural
  // bttRegistryPNatural() {
  //   document.getElementById('business').style.display = "none";
  //   document.getElementById('naturalPerson').style.display = "block";
  //   if (this.activate == true) {
  //     this.activate = false;
  //   }
  // }

  options = [
    {
      title: 'Cédula de Ciudadanía',
    },
    {
      title: 'Cédula de Extranjería',
    },
    {
      title: 'Carné diplomatico',
    },
    {
      title: 'Pasaporte',
    },
    {
      title: 'Permiso especial de permanencia',
    },
  ];
  Actividad = [
    {
      title: 'Agricultura y ganadería',
    },
    {
      title: 'Bienes de consumo',
    },
    {
      title: 'Comercio electrónico',
    },
    {
      title: 'Construcción',
    },
    {
      title: 'Alimentación',
    },
    {
      title: 'Deporte y ocio',
    },
    {
      title: 'Energía',
    },
    {
      title: 'Finanzas, seguros y bienes inmuebles',
    },
    {
      title: 'Lógistica y transporte',
    },
    {
      title: 'Medios de comunicación y marketing',
    },
    {
      title: 'Metalurgia y electrónica',
    },
    {
      title: 'Productos químicos y materias primas',
    },
    {
      title: 'Salud e industria farmacéutica',
    },
    {
      title: 'Educación',
    },
    {
      title: 'Tecnología y telecomunicaciones',
    },
    {
      title: 'Turismo y hotelería',
    },
    {
      title: 'Fabricación y manufacturación',
    },
  ];
  
}