import { Component, OnInit } from '@angular/core';
import { AppComponent } from 'src/app/app.component';
import { Router } from '@angular/router';
import { ModalController } from '@ionic/angular';
import { VideolabService } from 'src/app/core/service/videolab.service';
import { VideoPopupPage } from '../video-popup/video-popup.page';
import { ModalImagePage } from '../bankofprojects/projectsdetails/modal-image/modal-image.page';

@Component({
  selector: 'app-home',
  templateUrl: './home.page.html',
  styleUrls: ['./home.page.scss'],
})
export class HomePage implements OnInit {
  items=[];
  items2=[];
  // Activa los slides
  slideOpts = {
    initialSlide: 1,
    speed: 400,
    autoplay:true
    
  };
  user = this.app.userIsRegistered

  constructor(
    private app: AppComponent, public router: Router,
    private modalvideo: ModalController, 
    private videoservice: VideolabService,  
    private modalCtrl : ModalController
    // private app: MenuComponent, 

  ) { }
  async popUp(){
    const modal=this.modalCtrl.create({
    component:ModalImagePage,
    cssClass:'my-modal2-css',
    
 });
 return (await modal).present()
  }
  async videomodal(value: any){
    const modal=this.modalvideo.create({
    component:VideoPopupPage,
    cssClass:'my-modal-css',
    componentProps:{
      passurl:value
 }
 });
 return (await modal).present()
  }

  ngOnInit() { 
    this.items=this.videoservice.getvideo();
    this.items2=this.videoservice.getvideo2();
  }

}