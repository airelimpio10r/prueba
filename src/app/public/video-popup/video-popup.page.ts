import { Component, OnInit, Input } from '@angular/core';
import { SafeResourceUrl, DomSanitizer } from '@angular/platform-browser';
import { ModalController } from '@ionic/angular';

@Component({
  selector: 'app-video-popup',
  templateUrl: './video-popup.page.html',
  styleUrls: ['./video-popup.page.scss'],
})
export class VideoPopupPage implements OnInit {
  @Input()
  urlSafe: SafeResourceUrl;
    constructor(private modalvideo: ModalController, public dom: DomSanitizer) { }
  passurl:string;
    ngOnInit() {
      this.urlSafe=this.dom.bypassSecurityTrustResourceUrl(this.passurl);
    }

}
