import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { EmailComposer } from '@ionic-native/email-composer/ngx';
import { LogicReuseService } from 'src/app/shared/resources/logic-reuse.service';

@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.page.html',
  styleUrls: ['./contactus.page.scss'],
})
export class ContactusPage implements OnInit {

  FormContactanos: FormGroup;
  // Mesajes de error en las entradas de datos de los forms
  validation_messages;

  constructor(
    private emailComposer: EmailComposer, private logicReuse: LogicReuseService,
    private formBuilder: FormBuilder,
  ) { }

  ngOnInit() {
    this.FormContactanos = this.FormValid();
    this.validation_messages = this.logicReuse.validation_messages;
  }

  //Evalua el uso del botton enter para hacer el submit
  eventHandler(keyCode) {
    if (keyCode === 13) {
      if (this.FormContactanos.valid) {
        // Trigger the button element with a click
        document.getElementById("bttSubmitContactanos").click();
      }
    }
  }
  // Condiciones de la entrada de datos form
  private FormValid() {
    return this.formBuilder.group({
      name: ['', [Validators.required, Validators.pattern('[A-Za-z ]*'), Validators.minLength(2)]],
      lastName: ['', [Validators.required, Validators.pattern('[A-Za-z ]*'), Validators.minLength(2)]],
      email: ['', [Validators.required, Validators.email]],
      telephone: ['', [Validators.required, Validators.pattern('[0-9 ]*')]],
      bodymessage: ['', [Validators.required]]
    })
  }
  // Envia un mensaje por el correo electronico del usuario
  sendEmail(value) {
    let email = {
      to: '10rairelimpio@gmail.com',
      subject: 'PQR',
      body: 'Yo: ' + value.nombre + ' ' + value.apellido + '\n' + 'con correo electronico: ' + value.email + '\n' + 'y celular: ' + value.telefono + '\n' + 'El motivo o razón de consulta es: ' + value.bodymessage,
      isHtml: true
    }
    // Send a text message using default options
    this.emailComposer.open(email);
    this.FormContactanos.reset(); // borramos los datos del form
  }

}
