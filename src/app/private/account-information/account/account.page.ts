import { Component, OnInit, ChangeDetectorRef, ElementRef, ViewChild } from '@angular/core';
import { AuthService } from '../../../core/service/auth.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NavController, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { FirestoreCRUDService } from 'src/app/core/service/firestore/firestore-crud.service';
import { LogicReuseService } from 'src/app/shared/resources/logic-reuse.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.page.html',
  styleUrls: ['./account.page.scss'],
})
export class AccountPage implements OnInit {

  dataUser = this.CRUD.dataUser;
  // Data Form
  UpdatePNatural: FormGroup;
  // objetos vacios para llenar con los datos del select
  Departmentchoose = [];
  Municipalitychoose = [];
  countries;
  // Mesajes de error en las entradas de datos de los forms
  validation_messages;

  constructor(
    private router: Router, private authService: AuthService, private navCtrl: NavController,
    private formBuilder: FormBuilder, private _cdr: ChangeDetectorRef,
    private CRUD: FirestoreCRUDService, private logicReuse: LogicReuseService, 
  ) { }

  ngOnInit() {
    this.UpdatePNatural = this.FormPNatural();
    this.countries = this.logicReuse.countries;
    this.validation_messages = this.logicReuse.validation_messages;
  }

  //Busqueda y carga de imagen
  handleImage(event: any): void {
    const image = event.target.files[0];
    this.CRUD.updateImageProfile(image)
  }
  //Evalua el uso del botton enter para hacer el submit
  eventHandler(keyCode) {
    if (keyCode === 13) {
      if (this.UpdatePNatural.valid) {
        // Trigger the button element with a click
        document.getElementById("bttSubmit").click();
      }
    }
  }
  // Activa el evento ionChange para introducir datos a los selects
  oncountryChange() {
    let country = this.UpdatePNatural.get('country').value; //Pide el valor del Select pais
    this.Departmentchoose = this.logicReuse.CountryDepartment[country]; // introduce en el Select las opciones de acuerdo a lo escogido
    document.getElementById('department').style.display = "block";
    this._cdr.detectChanges();
  }
  departmentChange() {
    let department = this.UpdatePNatural.get('department').value;//Pide el valor del Select departamento
    this.Municipalitychoose = this.logicReuse.MunicipalDepartment[department];// introduce en el Select las opciones de acuerdo a lo escogido
    document.getElementById('municipality').style.display = "block";
    this._cdr.detectChanges();
  }
  // Donde se guardan los datos del form
  saveDataPNatural(record) {
    this.CRUD.updateProfile('users', record);
    this.UpdatePNatural.reset();
    this.router.navigate(['/home']);
  }
  // Condiciones de la entrada de datos form
  private FormPNatural() {
    return this.formBuilder.group({
      nit: ['', Validators.minLength(2)],
      companyName: ['', Validators.minLength(2)],
      companyActivity: ['', Validators.minLength(2)],
      documentType: ['', Validators.required],
      document: ['', [Validators.required, Validators.minLength(8), Validators.pattern('[0-9]*')]],
      name: ['', [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(2)]],
      lastName: ['', [Validators.required, Validators.pattern('[a-zA-Z ]*'), Validators.minLength(2)]],
      country: ['', Validators.required],
      department: ['', Validators.required],
      municipality: ['', Validators.required],
      cellPhone: ['', [Validators.required, Validators.minLength(10), Validators.maxLength(10), Validators.pattern('[0-9]*')]],
      telephone: ['', [Validators.required, Validators.minLength(7), Validators.maxLength(7), Validators.pattern('[0-9]*')]],
      direction: ['', [Validators.required, Validators.minLength(2)]]
    });
  }
}






// import { Injectable } from '@angular/core';
// import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
// import { Observable } from 'rxjs';
// import { map, finalize } from 'rxjs/operators';
// import { PostI } from '../../shared/models/post.interface';
// import { FileI } from '../../shared/models/file.interface';
// import { AngularFireStorage } from '@angular/fire/storage';

// @Injectable({
//   providedIn: 'root'
// })
// export class PostSelrvice {
//   private postsCollection: AngularFirestoreCollection<PostI>;
//   private filePath: any;
//   private downloadURL: Observable<string>;

//   constructor(
//     private afs: AngularFirestore,
//     private storage: AngularFireStorage
//   ) {
//     this.postsCollection = afs.collection<PostI>('posts');
//   }

//   public preAddAndUpdatePost(post: PostI, image: FileI): void {
//     this.uploadImage(post, image);
//   }

//   private savePost(post: PostI) {
//     const postObj = {
//       titlePost: post.titlePost,
//       contentPost: post.contentPost,
//       imagePost: this.downloadURL,
//       fileRef: this.filePath,
//       tagsPost: post.tagsPost
//     };
//     if (post.id) {
//       return this.postsCollection.doc(post.id).update(postObj);
//     } else {
//       return this.postsCollection.add(postObj);
//     }
//   }

//   private uploadImage(post: PostI, image: FileI) {
//     this.filePath = `images/${image.name}`;
//     const fileRef = this.storage.ref(this.filePath);
//     const task = this.storage.upload(this.filePath, image);
//     task.snapshotChanges()
//       .pipe(
//         finalize(() => {
//           fileRef.getDownloadURL().subscribe(urlImage => {
//             this.downloadURL = urlImage;
//             this.savePost(post);
//           });
//         })
//       ).subscribe();
//   }
// }