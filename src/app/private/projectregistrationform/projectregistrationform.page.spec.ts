import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ProjectregistrationformPage } from './projectregistrationform.page';

describe('ProjectregistrationformPage', () => {
  let component: ProjectregistrationformPage;
  let fixture: ComponentFixture<ProjectregistrationformPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProjectregistrationformPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ProjectregistrationformPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
