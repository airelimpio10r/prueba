import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { NavController, ToastController } from '@ionic/angular';
import { Router } from '@angular/router';
import { LogicReuseService } from 'src/app/shared/resources/logic-reuse.service';
import { FirestoreCRUDService } from 'src/app/core/service/firestore/firestore-crud.service';

@Component({
  selector: 'app-projectregistrationform',
  templateUrl: './projectregistrationform.page.html',
  styleUrls: ['./projectregistrationform.page.scss'],
})
export class ProjectregistrationformPage implements OnInit {

  RegisterProject: FormGroup;
  // objetos vacios para llenar con los datos del select country department y municipality
  Departmentchoose = [];
  Municipalitychoose = [];
  countries;
  // Mesajes de error en las entradas de datos de los forms
  validation_messages = this.logicReuse.validation_messages
  // objetos vacios para llenar con los datos del select 
  typepfprojects = ['Conservación', 'Creación'];
  foresttype = ['Páramo', 'Bosque', 'Humedal'];
  temperaturetype = ['°C', '°F'] //Fahrenheit  Celsius​
  Currentusage = ['Recreación', 'Turismo', 'Senderismo', 'Abandonado', 'Siembra'];
  zone = ['Rural', 'Urbano'];
  treespecies = ['Ceiba', 'Yarumo', 'Caracoí', 'Guayacán amarrillo', 'Arrayan', 'Almendro', 'Cedro', 'Roble'];
  treesize = ['1-5 mts', '5-10 mts', '10-20 mts', '20-30 mts', '30-50 mts', '50 mts o más'];
  weather = ['Frio', 'Templado', 'Seco', 'Tropical'];
  floortype = ['Arcilla', 'Limo', 'Grava'];
  ageoftree = ['0-5 años', '5-10 años', '10-20 años', '20-40 años', '40 años o más'];
  heightsealevel = ['1000-1500 mts', '1500-2500 mts', '2500-3500 mts', '3500 mts o más'];
  treediameter = ['10-15 cm', '15-25 cm', '25-35 cm', '35-50 cm', '1-2 mts', '2-4 mts', '4 mts o más'];
//
  imagePreview: any = [];
  imageCharge: any = [];

  constructor(
    private CRUD: FirestoreCRUDService, private navCtrl: NavController,
    private toastController: ToastController, private router: Router,
    private formBuilder: FormBuilder, private _cdr: ChangeDetectorRef,
    private logicReuse: LogicReuseService
  ) { }

  ngOnInit() {
    this.RegisterProject = this.FromRegisterProject();
    this.countries = this.logicReuse.countries;
    this.validation_messages = this.logicReuse.validation_messages;
  }

  //Busqueda y carga de imagen
  handleImage(event: any): void {
    this.imageCharge.push(event.target.files[0]); 
    if (event.target.files && event.target.files[0]) {
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]); // read file as data url
      reader.onload = (event) => { // called once readAsDataURL is completed
        this.imagePreview.push(event.target["result"]);
      }
    }
  }
  //Evalua el uso del botton enter para hacer el submit
  eventHandler(keyCode) {
    if (keyCode === 13) {
      if (this.RegisterProject.valid) {
        // Trigger the button element with a click
        document.getElementById("bttSubmitResgistroProyecto").click();
      }
    }
  }
  // Donde se guardan los datos del form
  saveData(value) {
    this.CRUD.registerProject(value, this.imageCharge);
    this.RegisterProject.reset(); // borramos los datos del form
    this.router.navigate(['/home']);
  }
  // Condiciones de la entrada de datos form
  private FromRegisterProject() {
    return this.formBuilder.group({
      projectType: ['', Validators.required],
      projectName: ['', Validators.required],
      forestArea: ['', [Validators.required, Validators.pattern('[0-9]*'), Validators.min(1)]],
      forestType: ['', Validators.required],
      temperature: ['', [Validators.required, Validators.pattern('[0-9]*')]],
      typeTemperature: ['', Validators.required],
      treeDiameter: ['', Validators.required],
      treeAge: ['', Validators.required],
      treeSize: ['', Validators.required],
      country: ['', Validators.required],
      department: ['', Validators.required],
      municipality: ['', Validators.required],
      zone: ['', Validators.required],
      msnm: ['', [Validators.required]],
      propertyName: ['', Validators.required],
      cadastralCertificate: ['', Validators.required],
      enrollment: ['', Validators.required],
      treespecies: ['', Validators.required],
      uploadImage: ['', Validators.required],
    });
  }
  // Activa el evento ionChange para introducir datos a los selects
  oncountryChange() {
    let country = this.RegisterProject.get('country').value; //Pide el valor del Select pais
    this.Departmentchoose = this.logicReuse.CountryDepartment[country]; // introduce en el Select las opciones de acuerdo a lo escogido
    document.getElementById('department').style.display = "block";
    this._cdr.detectChanges();
  }
  departmentChange() {
    let department = this.RegisterProject.get('department').value;//Pide el valor del Select departamento
    this.Municipalitychoose = this.logicReuse.MunicipalDepartment[department];// introduce en el Select las opciones de acuerdo a lo escogido
    document.getElementById('municipality').style.display = "block";
    this._cdr.detectChanges();
  }
}
