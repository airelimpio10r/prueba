import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProjectregistrationformPageRoutingModule } from './projectregistrationform-routing.module';

import { ProjectregistrationformPage } from './projectregistrationform.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProjectregistrationformPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [ProjectregistrationformPage]
})
export class ProjectregistrationformPageModule {}
