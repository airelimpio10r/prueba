import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FirestoreCRUDService } from 'src/app/core/service/firestore/firestore-crud.service';
import { Router } from '@angular/router';
import { NavController, AlertController } from '@ionic/angular';
import { LogicReuseService } from 'src/app/shared/resources/logic-reuse.service';
import { Sponsor } from 'src/app/shared/patrocinio.interface';

@Component({
  selector: 'app-sponsorshipform',
  templateUrl: './sponsorshipform.page.html',
  styleUrls: ['./sponsorshipform.page.scss'],
})
export class SponsorshipformPage implements OnInit {

  dataSponsorship: FormGroup;
  // Mesajes de error en las entradas de datos de los forms
  validation_messages = this.logicReuse.validation_messages
  currentYear; sponsorshipvalue;
  objectsToSponsor = ['Número de árboles', 'Hectáreas de bosque'];
  contributionFrequency = ['Mensual', 'Trimestral', 'Semestral', 'Anual']
  valueContribution = { 'Mensual': 1, 'Trimestral': 3, 'Semestral': 6, 'Anual': 12 }
  // FIXME: sE DEBE CREAR OBJETOS PARA GUARDAR LOS DATOS DE LOS SELECTS
  constructor(
    private CRUD: FirestoreCRUDService, private router: Router, private navCtrl: NavController,
    private formBuilder: FormBuilder, private logicReuse: LogicReuseService,
    private _cdr: ChangeDetectorRef
  ) {
    // Le entrega un formato de inicio a los ion data time 
    let date = new Date();
    this.currentYear = date.getFullYear() + '-' + ('0' + (date.getMonth() + 1)).slice(-2) + '-' + ('0' + date.getDate()).slice(-2);
  }

  ngOnInit() {
    this.dataSponsorship = this.FromAspLegales();
  }

  amountSponsorChange() {
    let amountSponsor = this.dataSponsorship.get('amountSponsor').value;
    let quantity: number = this.dataSponsorship.get('quantity').value;
    let contributionFrequency = this.dataSponsorship.get('contributionFrequency').value;
    const timeOfContribution: number = this.valueContribution[contributionFrequency];
    let amountToBePaid;
    if (amountSponsor == 'Numero de arboles') {
      amountToBePaid = (quantity * 9000)*timeOfContribution
    } else {
      amountToBePaid = (quantity * 2700000)*timeOfContribution
    }
    this.sponsorshipvalue = amountToBePaid
    this._cdr.detectChanges();
  }

  //FIXME: Devulve la fecha en formato ISO (Fecha+Zona+Hora)
  // Donde se guardan los datos del form
  saveData(value) {
    this.CRUD.createSponsorship(value, this.sponsorshipvalue);
    this.dataSponsorship.reset(); // borramos los datos del form
    this.logicReuse.alertMessaje('<img src="assets/img/metodos_de_pago.jpg">')
    this.router.navigate(['/home']);
  }
  // Condiciones de la entrada de datos form
  private FromAspLegales() {
    return this.formBuilder.group({
      sponsorshipStart: ['', Validators.required],
      endSponsorship: ['', Validators.required],
      amountMoney: [''],
      contributionFrequency: ['', Validators.required],
      projectName: ['', Validators.required],
      amountSponsor: [''],
      quantity: [''] //FIXME: Arregla la entrada de datos y el mensaje de validación 
    });
  }
}
