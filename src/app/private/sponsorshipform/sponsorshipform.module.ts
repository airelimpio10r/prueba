import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SponsorshipformPageRoutingModule } from './sponsorshipform-routing.module';

import { SponsorshipformPage } from './sponsorshipform.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SponsorshipformPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [SponsorshipformPage]
})
export class SponsorshipformPageModule {}
