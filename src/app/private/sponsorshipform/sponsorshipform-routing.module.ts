import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SponsorshipformPage } from './sponsorshipform.page';

const routes: Routes = [
  {
    path: '',
    component: SponsorshipformPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SponsorshipformPageRoutingModule {}
