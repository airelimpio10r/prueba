import { Injectable } from '@angular/core';

import { GooglePlus } from '@ionic-native/google-plus/ngx';
import { AuthService } from '../../service/auth.service';
import { AngularFirestore, AngularFirestoreDocument } from "@angular/fire/firestore";
import { auth } from 'firebase/app';
import { AngularFireAuth } from "@angular/fire/auth";
import { User } from 'src/app/shared/user.interface';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class GoogleService {


  constructor(
    private googleplus: GooglePlus, private authServise: AuthService,
    private AFauth: AngularFireAuth, private dataBase: AngularFirestore,
    private router: Router
  ) { }

  updateAccount(user) {
    this.dataBase.doc<User>(`users/${user.email}`).valueChanges().subscribe(docData => {
      if (docData.document) {
        this.router.navigate(['/home']);
      } else {
        this.router.navigate(['/account']);
      }
    });
  }
  // login with google 
  async loginWithGoogle(): Promise<User> {
    try {
      const { user } = await this.googleplus.login({}).then(result => {
        const user_Data_Google = result;
        return this.AFauth.auth.signInWithCredential(auth.GoogleAuthProvider.credential(null, user_Data_Google.accessToken));
      });
      const data = this.AFauth.auth.currentUser
      this.updateUserData(data);
      this.updateAccount(data);
      return user;
    } catch (error) {
      this.WebLoginWithGoogle();
    }
  }
  //login with google  para navegadores web
  async WebLoginWithGoogle(): Promise<User> {
    try {
      const { user } = await this.AFauth.auth.signInWithPopup(new auth.GoogleAuthProvider());
      this.updateUserData(user);
      this.updateAccount(user)
      return user;
    } catch (error) {
      console.log('Error->', error);
      this.authServise.alertMessaje(error.message);
      console.log('Error on Login user', error);
      alert(error);
    }
  }
  // Recupera la información que trae la cuenta de google y Actualiza datos del usuario registrado
  public updateUserData(user) {
    try {
      const data: User = {
        uid: user.uid,
        name: user.displayName,
        photoProfile: {
          photoURL: user.photoURL,
        },
        email: user.email,
        rol: 'P.Natural'
      };
      const userRef: AngularFirestoreDocument<User> = this.dataBase.doc(`users/${user.email}`);
      return userRef.set(Object.assign({}, data), { merge: true });
    } catch (error) {
      console.log('Error->', error);
      this.authServise.alertMessaje(error.message);
      console.log('Error on Login user', error);
      alert(error);
    }
  }
  //cerrar sesión 
  async logout(): Promise<void> {
    try {
      await this.AFauth.auth.signOut();
      this.googleplus.disconnect();
    } catch (error) {
      this.authServise.alertMessaje(error.message);
    }
  }
}
