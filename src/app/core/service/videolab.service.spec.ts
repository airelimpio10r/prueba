import { TestBed } from '@angular/core/testing';

import { VideolabService } from './videolab.service';

describe('VideolabService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: VideolabService = TestBed.get(VideolabService);
    expect(service).toBeTruthy();
  });
});
