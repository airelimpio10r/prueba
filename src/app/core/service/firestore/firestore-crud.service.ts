import { Injectable } from '@angular/core';
import { AuthService } from '../auth.service';
import { AngularFirestore, AngularFirestoreDocument } from "@angular/fire/firestore";
import { User } from 'src/app/shared/user.interface';
import { RegisterProject, IType } from 'src/app/shared/registrarproyecto.interface';
import { Sponsor } from 'src/app/shared/patrocinio.interface';
import { AngularFireStorage } from '@angular/fire/storage';
import { finalize } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FirestoreCRUDService {

  dataUser: any = [];

  constructor(
    private authService: AuthService, private dataBase: AngularFirestore,
    private storage: AngularFireStorage,
  ) {
    this.readDataUser();
  }
  // Registar un proyecto en el banco de proyectos
  public registerProject(record, image) {
    const projectBankImages = {}
    image.forEach(element => {
      const randomId = Math.random().toString(36).substring(2, 8);
      const filePath = `project_bank_images/${randomId}`;
      const fileRef = this.storage.ref(filePath);
      const task = this.storage.upload(filePath, element);
      task.snapshotChanges()
        .pipe(
          finalize(() => {
            fileRef.getDownloadURL().subscribe(urlImage => {
              projectBankImages[randomId] = urlImage;
            });
          })
        ).subscribe();
    });
    const data: RegisterProject = {
      projectType: record.projectType,
      projectName: record.projectName,
      forestArea: record.forestArea,
      forestType: record.forestType,
      temperature: record.temperature,
      typeTemperature: record.typeTemperature,
      treeDiameter: record.treeDiameter,
      treeAge: record.treeAge,
      treeSize: record.treeSize,
      country: record.country,
      department: record.department,
      municipality: record.municipality,
      zone: record.zone,
      msnm: record.msnm,
      propertyName: record.propertyName,
      cadastralCertificate: record.cadastralCertificate,
      enrollment: record.enrollment,
      user: {
        email: this.authService.emailUser,
      },
      photos: projectBankImages // FIXME: ME CARGA LA DATA PERO NO ME LA GUARDA COMO DEBERIA el objeto de imagenes
    }
    const userRef: AngularFirestoreDocument<RegisterProject> = this.dataBase.doc(`registerProject/${record.projectName}`);
    return userRef.set(Object.assign({}, data), { merge: true });
    // const data = {
    //   photos: projectBankImages,
    // }
    // console.log(projectBankImages)
    // this.dataBase.doc(`registerProject/${record.projectName}`).set(Object.assign({}, data),{merge: true});
  }
  //Actualizar imagen de perfil 
  public updateImageProfile(image) {
    try {
      let uidPhoto
      this.dataUser.forEach(element => {
        uidPhoto = element.photoProfile.uidPhoto
      });
      if (uidPhoto == undefined) {
        // Genera al azar un UID 
        const randomId = Math.random().toString(36).substring(2, 8);
        this.uploadImage(randomId, image)
      } else {
        this.uploadImage(uidPhoto, image)
      }
    } catch (error) {
      console.log('error ->', error)
    }
  }
  // Carga la url del storage y lo guarda en la base de datos
  public uploadImage(uidImage, image) {
    try {
      const filePath = `profile_picture/${uidImage}`;
      const fileRef = this.storage.ref(filePath);
      const task = this.storage.upload(filePath, image);
      task.snapshotChanges()
        .pipe(
          finalize(() => {
            fileRef.getDownloadURL().subscribe(urlImage => {
              const data: User = {
                photoProfile: {
                  photoURL: urlImage,
                  uidPhoto: uidImage
                }
              }
              const userRef: AngularFirestoreDocument<User> = this.dataBase.doc(`users/${this.authService.emailUser}`);
              return userRef.update(Object.assign({}, data));
            });
          })
        ).subscribe();
    } catch (error) {
      console.log('error ->', error)
    }
  }
  //Leer datos de usuario
  private readDataUser() {
    try {
      this.authService.user$.subscribe(docData => {
        this.dataUser.pop();
        this.dataUser.push(docData);
      });
    } catch (error) {
      console.log('error ->', error)
    }
  }
  // Actualizar datos del perfil
  public updateProfile(collection, record) {
    try {
      const data: User = {
        name: record.name,
        lastName: record.lastName,
        nit: record.nit,
        companyName: record.companyName,
        companyActivity: record.companyActivity,
        documentType: record.documentType,
        document: record.document,
        cellPhone: record.cellPhone,
        direction: record.direction,
        country: record.country,
        department: record.department,
        municipality: record.municipality,
        telephone: record.telephone,
      };
      const userRef: AngularFirestoreDocument<User> = this.dataBase.collection(collection).doc(this.authService.emailUser);
      return userRef.update(Object.assign({}, data));
    } catch (error) {
      console.log('error ->', error)
    }
  }
  // registar un proyecto y registra el patrocinio
  public createSponsorship(record, sponsorshipvalue) {
    try {
      const data: Sponsor = {
        quantity: record.quantity,
        sponsorshipStart: record.sponsorshipStart, // FIXME: ARREGLAR EL FORMATO DE FECHA Y HORA
        endSponsorship: record.endSponsorship,
        amountSponsor: record.amountSponsor,
        amountMoney: sponsorshipvalue,
        contributionFrequency: record.contributionFrequency,
        projectName: record.projectName,
        user: {
          email: this.authService.emailUser
        }
      }
      const userRef: AngularFirestoreDocument<Sponsor> = this.dataBase.doc(`sponsorship/${this.authService.emailUser}/sponsorship/${record.projectName}`);
      return userRef.set(Object.assign({}, data), { merge: true });
    } catch (error) {
      console.log('error ->', error)
    }
  }

  // private delete(collection,record_id) {
  //   this.dataBase.doc(collection + '/' + record_id).delete();
  // }
}
