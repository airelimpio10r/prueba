import { Injectable } from '@angular/core';

import { AngularFireAuth } from "@angular/fire/auth";
import { Router } from '@angular/router';
import { AngularFirestore, AngularFirestoreDocument } from "@angular/fire/firestore";
import { AlertController } from '@ionic/angular';
import { Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { User } from '../../shared/user.interface';
import { auth } from 'firebase/app';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public user$: Observable<User>;
  public emailUser: string

  constructor(
    private AFauth: AngularFireAuth, private alertController: AlertController,
    private router: Router, private dataBase: AngularFirestore,
  ) {
    this.stateLogin();
  }
  //Observa el estado de autentificación
  public stateLogin() {
    this.user$ = this.AFauth.authState.pipe(
      switchMap((user) => {
        if (user) {
          this.emailUser = user.email;
          //Mantiene el login
          this.AFauth.auth.setPersistence(auth.Auth.Persistence.LOCAL);
          console.log("The user is log in!");
          return this.dataBase.doc<User>(`users/${user.email}`).valueChanges();
        }
        console.log("The user is not logged in!");
        return of(null);
      })
    );
  }
  //Reset de contraseña por medio de email
  async resetPassword(data): Promise<void> {
    try {
      return this.AFauth.auth.sendPasswordResetEmail(data.email);
    } catch (error) {
      console.log(error);
    }
  }
  // Login with email and password
  async onLogin(value): Promise<{}> {
    try {
      const { user } = await this.AFauth.auth.signInWithEmailAndPassword(value.email, value.password);
      this.redirectUser(user);
      return user;
    } catch (error) {
      this.alertMessaje(error.message);
    }
  }
  // Redirigir usuario
  public redirectUser(isVerified): void {
    if (!isVerified.emailVerified) {
      this.AFauth.auth.currentUser.sendEmailVerification();
      this.alertMessaje('Revisa tu correo electronico en bandeja de entrada o spam para validar su registro');
      this.alertMessaje('Su usuario no ha sido verificado. Reenviaremos el link de verificación');
      this.AFauth.auth.signOut();
      document.getElementById('bttSubmit').textContent = 'Reenviar el link de verificación';
    }
    else {

      this.router.navigate(['/home']);
    }
  }
  //Register with email and password
  async onRegister(data): Promise<{}> {
    try {
      const { user } = await this.AFauth.auth.createUserWithEmailAndPassword(data.email, data.passwordRetry.passwordConfirmation);
      this.saveDataOtherusers(data);
      this.AFauth.auth.currentUser.sendEmailVerification();
      this.alertMessaje('Por favor actualiza tus datos antes de continuar');
      this.router.navigate(['/account']);
      this.alertMessaje('Revisa tu correo electronico en bandeja de entrada o spam para validar su registro');
      return user;
    } catch (error) {
      this.alertMessaje(error.message);
    }
  }
  // Actualiza datos del usuario registrado por correo y contraseña 
  private saveDataOtherusers(user) {
    try {
      const user_Data = this.AFauth.auth.currentUser;
      if (user.nit) {
        const data: User = {
          uid: user_Data.uid,
          name: user.name + ' ' + user.lastName,
          photoProfile: {
            photoURL:'https://firebasestorage.googleapis.com/v0/b/database-1a5ff.appspot.com/o/profile_picture%2FdefaultImage.png?alt=media&token=c2ef1066-fafd-42c4-bd55-15fb8f11dc26',
          },
          email: user.email,
          rol: 'Empresa',
          nit: user.nit,
          companyName: user.companyName,
          companyActivity: user.companyActivity,
          documentType: user.documentType,
          document: user.document,
          cellPhone: user.cellPhone,
          direction: user.direction,
        };
        const userRef: AngularFirestoreDocument<User> = this.dataBase.doc(`users/${user.email}`);
        return userRef.set(Object.assign({}, data), { merge: true });
      } else {
        const data: User = {
          uid: user_Data.uid,
          name: user.name + ' ' + user.lastName,
          photoProfile: {
            photoURL:'https://firebasestorage.googleapis.com/v0/b/database-1a5ff.appspot.com/o/profile_picture%2FdefaultImage.png?alt=media&token=c2ef1066-fafd-42c4-bd55-15fb8f11dc26',
          },
          email: user.email,
          rol: 'P.Natural',
        };
        const userRef: AngularFirestoreDocument<User> = this.dataBase.doc(`users/${user.email}`);
        return userRef.set(Object.assign({}, data), { merge: true });
      }
    } catch (error) {
      console.log('Error->', error);
      this.alertMessaje(error.message);
      console.log('Error on Login user', error);
      alert(error);
    }
  }
  //Mensaje de Validación de registro
  async alertMessaje(message: string) {
    const alert = await this.alertController.create({
      cssClass: 'my-custom-class',
      header: 'Confirm!',
      message: message,
      buttons: [
        {
          text: 'Okay',
        }
      ]
    });
    await alert.present();
  }
}