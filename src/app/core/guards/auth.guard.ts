import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { AuthService } from '../../core/service/auth.service';
import { take, map } from 'rxjs/operators';
import { identifierModuleUrl } from '@angular/compiler';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService, private router: Router) { }
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    const expectedRole = next.data.role;
    return this.authService.user$.pipe(
      take(1),
      map(user => {
        if (user) {
          if (expectedRole == undefined) {
            return true;
          } else if (expectedRole == user.rol) {
            return true;
          }
          // RedirectToLoginPage
          this.router.navigate(['/login'])
          return false;
        } else {
          // RedirectToLoginPage
          this.router.navigate(['/login'])
          return false;
        }
      })
    )
  }

}
