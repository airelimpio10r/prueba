import { Injectable } from '@angular/core';
import { CanLoad, Route, UrlSegment, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CheckTutorialGuard implements CanLoad {
  
  constructor(private router: Router) {}
  ckeckTutorial: boolean;
  canLoad(
    route: Route,
    segments: UrlSegment[]): Observable<boolean> | Promise<boolean> | boolean {
      if (this.ckeckTutorial == true) {
        this.router.navigate(['/home']);
        return false; //FIXME: Cargar una variabla que cambie y se cuarde aun que se cierre el programa 
      } else {
        return true;
      }
  }
}
